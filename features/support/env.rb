require "appium_lib"

def caps
  {
      caps: {
      deviceName: "Galaxy S6",
      platformName: "Android",
      app: (File.join(File.dirname(__FILE__),"")),
      appPackage: "com.kaspersky.secure.connection",
      appActivity: "com.kaspersky.remote.security_service.WizardActivityLauncher",
      newCommandTimeout: "3600"
  }
  }
end

Appium::Driver.new(caps, true)
Appium.promote_appium_methods Object